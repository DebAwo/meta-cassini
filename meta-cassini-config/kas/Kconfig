# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

mainmenu "Cassini Reference Stack Build Config"

#############################
# User-facing configuration #
#############################
comment "Target Build Setup"

choice
    prompt "Build Image Type"
    default CASSINI_IMAGE
    help
     Select the image type you would like to build from the Cassini distribution.

config CASSINI_IMAGE
    bool "Standard Cassini Compliant Image"
    help
     Build an image for the Cassini distribution.

config CASSINI_DEVELOPMENT_IMAGE
    bool "Development Enabled Cassini Compliant Image"
    help
     Build a Cassini image suitable for development (e.g. allowing
     root login without password)

config CASSINI_SDK_IMAGE
    bool "Cassini Compliant SDK"
    help
     Build a Cassini image with additional tools for software development.

endchoice

menu "Platform Selection"
choice
    prompt "Build Platform"
    default CORSTONE1000_FVP
    help
     Select the build platform you would like your image to run on

config CORSTONE1000_FVP
    bool "Corstone-1000 FVP"
    help
     Build targeting Arm Corstone-1000 FVP

config CORSTONE1000_MPS3
    bool "Corstone-1000 MPS3"
    help
     Build targeting Arm Corstone-1000 for MPS3

config N1SDP
    bool "N1SDP"
    help
     Build targeting Arm Neoverse N1 System Development Platform

endchoice

comment "End-User License Agreement"
    depends on CORSTONE1000_FVP

config ARM_FVP_EULA_ACCEPT_CHOICE
    depends on CORSTONE1000_FVP
    bool "Accept the END USER LICENSE AGREEMENT FOR ARM SOFTWARE DEVELOPMENT TOOLS"
    default n
    help
      Please refer to "https://developer.arm.com/downloads/-/arm-ecosystem-fvps/eula"
endmenu


menu "Extra Image Features"

config EXTRA_IMAGE_FEATURES_SECURITY
    bool "Security Hardened"
    default n
    help
     To build a security-hardened Cassini distribution image.

config EXTRA_IMAGE_FEATURES_TESTS
    bool "Include Integration Tests"
    default n
    help
     To include run-time validation tests into the image.

config RUN_TESTS
    depends on CORSTONE1000_FVP && EXTRA_IMAGE_FEATURES_TESTS
    bool "Run the tests automatically"
    default n
    help
     Run the integration tests on the selected target after
     building the image.

endmenu

##############
# Set up Kas #
##############

config KAS_INCLUDE_IMAGE
    string
    default "meta-cassini-config/kas/cassini.yml" if CASSINI_IMAGE
    default "meta-cassini-config/kas/cassini-dev.yml" if CASSINI_DEVELOPMENT_IMAGE
    default "meta-cassini-config/kas/cassini-sdk.yml" if CASSINI_SDK_IMAGE

config KAS_INCLUDE_PLATFORM
    string
    default "meta-cassini-config/kas/corstone1000-fvp.yml" if CORSTONE1000_FVP
    default "meta-cassini-config/kas/corstone1000-mps3.yml" if CORSTONE1000_MPS3
    default "meta-cassini-config/kas/n1sdp.yml" if N1SDP

config KAS_INCLUDE_SECURITY
    string
    default "meta-cassini-config/kas/security.yml" if EXTRA_IMAGE_FEATURES_SECURITY

config KAS_INCLUDE_TESTS
    string
    default "meta-cassini-config/kas/tests.yml" if EXTRA_IMAGE_FEATURES_TESTS

if RUN_TESTS
    config TESTIMAGE_AUTO
        string
        default "1" if RUN_TESTS
endif

config FVP_CORSTONE1000_EULA_ACCEPT
    string
    default "1" if ARM_FVP_EULA_ACCEPT_CHOICE
    default "0"

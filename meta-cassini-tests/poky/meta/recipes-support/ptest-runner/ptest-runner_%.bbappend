# SPDX-FileCopyrightText: Copyright (c) 2023, Linaro Limited.
#
# SPDX-License-Identifier: MIT

# Fetch ptest-runner 2.4.2 release tag
SRCREV:cassini = "bcb82804daa8f725b6add259dcef2067e61a75aa"

# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SUMMARY = "JFrog command line client application"
HOMEPAGE = "https://github.com/jfrog/jfrog-cli"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://src/${GO_IMPORT}/LICENSE;md5=dc7f21ccff0f672f2a7cd6f412ae627d"

SRC_URI = "git://${GO_IMPORT};branch=v2;name=${BPN};protocol=https"

# Points to 2.20.0 tag
SRCREV_${BPN} = "ca9d81ce27a519cb38898d73be1d94d207e5ecf6"

S = "${WORKDIR}/git"

GO_IMPORT = "github.com/jfrog/jfrog-cli"
SRCREV_FORMAT = "${PN}"

inherit go-mod

GO_INSTALL = "${GO_IMPORT}"

require src_uri.inc

RDEPENDS:${PN}-dev += "bash make"

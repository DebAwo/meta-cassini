# Copyright (c) 2022-2023 Arm Limited and/or its affiliates.
# <open-source-office@arm.com>
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend:libc-glibc:cassini := "${THISDIR}/files:"

QUIET_PRINTK = "20-quiet-printk.conf"

SRC_URI:append:libc-glibc:cassini = "file://${QUIET_PRINTK}"

do_install:append:libc-glibc:cassini() {
    install -Dm 0640 ${WORKDIR}/${QUIET_PRINTK} \
        ${D}${sysconfdir}/sysctl.d/${QUIET_PRINTK}
}

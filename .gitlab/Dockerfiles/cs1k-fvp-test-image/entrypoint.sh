#!/bin/bash
# Copyright (c) 2023 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

# Start libvirt daemon
if [ -z "$SKIP_LIBVIRT" ] ; then
    /usr/sbin/libvirtd &
fi

# Continue with specified command
exec "$@"

# Copyright (c) 2023 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT
ARG DOCKERHUB_MIRROR=

FROM ${DOCKERHUB_MIRROR}ubuntu:focal

ARG MODEL="Corstone-1000-23"
ARG MODEL_CODE="FVP_Corstone_1000"
ARG MODEL_VERSION="11.19_21"
ARG FVP_ARCH="Linux64"

RUN apt-get update \
    && apt-get install --no-install-recommends --yes \
    bc \
    ca-certificates \
    curl \
    gcc-8 \
    gnupg2 \
    libatomic1 \
    libdbus-1-3 \
    libssl1.1 \
    libvirt-clients \
    libvirt-daemon-system \
    telnet \
    tree \
    software-properties-common \
    unzip \
    wget \
    && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 60C317803A41BA51845E371A1E9377A2BA9EF27F \
    && add-apt-repository ppa:ubuntu-toolchain-r/test \
    && apt-get install --only-upgrade libstdc++6 -y \
    && apt-get clean \
    && rm -rf /var/cache/apt

# Create model directory
RUN mkdir /opt/model

# Setup networking
COPY network /etc/libvirt/hooks/
COPY entrypoint.sh /
RUN chmod 755 /entrypoint.sh \
    && chmod 755 /etc/libvirt/hooks/network
ENTRYPOINT ["/entrypoint.sh"]

# Add FVP and Testing Binaries
ADD "https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/${MODEL}/Linux/${MODEL_CODE}_${MODEL_VERSION}_${FVP_ARCH}.tgz" /tmp/

RUN cd /tmp \
    && tar -zxvf "/tmp/${MODEL_CODE}_${MODEL_VERSION}_${FVP_ARCH}.tgz" \
    && ./FVP_Corstone_1000.sh --i-agree-to-the-contained-eula \
        --no-interactive \
        --destination /opt \
    && rm -rf /tmp/*

EXPOSE 5000/tcp
EXPOSE 5001/tcp
EXPOSE 5002/tcp
EXPOSE 5003/tcp

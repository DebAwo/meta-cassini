# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# FFA driver
MACHINE_FEATURES:append = " arm-ffa"

# Trusted services secure partitions
MACHINE_FEATURES:append = " ts-crypto ts-storage ts-its ts-attestation ts-block-storage"

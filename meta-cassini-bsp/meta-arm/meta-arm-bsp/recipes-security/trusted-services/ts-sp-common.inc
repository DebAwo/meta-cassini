# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Common part of all Trusted Services SPs recipes

# From meta-arm
require recipes-security/trusted-services/ts-sp-common.inc

# Local overrides
require ts-uuid.inc
